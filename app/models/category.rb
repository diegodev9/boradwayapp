class Category < ApplicationRecord
  has_many :plays

  scope :list_all_for_options, -> { Category.all.map { |c| [c.name, c.id] } }
end

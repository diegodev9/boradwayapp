class Play < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :reviews
  has_one_attached :image do |attachable|
    attachable.variant :thumb, resize_to_limit: [250, 350]
  end

  validates :image, content_type: %w[image/png image/jpg image/jpeg image/webp], size: {
    less_than: 2.megabytes, message: 'must be less than 2MB'
  }

  scope :by_category, ->(category) { where(category_id: category) }
end

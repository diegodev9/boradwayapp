class PlaysController < ApplicationController
  before_action :authenticate_user!, only: %i[new edit]
  before_action :set_play, only: %i[show edit update destroy]
  before_action :set_categories, only: %i[new edit create update]

  def index
    if params[:category]
      category = Category.find_by(name: params[:category])
      @plays = Play.by_category(category)
    else
      @plays = Play.all.order(created_at: :desc)
    end
  end

  def show
    return @average_review = 0 if @play.reviews.blank?

    @average_review = @play.reviews.average(:rating).round(2)
    @half_star = true if @average_review.to_s.split('.').last != '0'
  end

  def new
    @play = current_user.plays.build
  end

  def edit
  end

  def create
    @play = current_user.plays.build(play_params)

    if @play.save
      redirect_to root_path
    else
      render 'new'
    end
  end

  def update
    @play.category_id = params[:category_id]

    if @play.update(play_params)
      redirect_to play_path(@play)
    else
      render :edit
    end
  end

  def destroy
    @play.destroy
    redirect_to root_path
  end

  private

  def set_play
    @play = Play.find(params[:id])
  end

  def set_categories
    @categories = Category.list_all_for_options
  end

  def play_params
    params.require(:play).permit(:title, :description, :director, :category_id, :image)
  end
end

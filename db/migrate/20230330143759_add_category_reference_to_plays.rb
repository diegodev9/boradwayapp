class AddCategoryReferenceToPlays < ActiveRecord::Migration[6.1]
  def change
    add_reference :plays, :category, foreign_key: true
  end
end

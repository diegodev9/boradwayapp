class AddUserReferenceToPlays < ActiveRecord::Migration[6.1]
  def change
    add_reference :plays, :user, foreign_key: true
  end
end
